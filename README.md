# code-challenges
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/608c55f6f4fe4207908aac774c77ad8c)](https://www.codacy.com/manual/alexkyo7463/code-challenges/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=Rudecat/code-challenges&amp;utm_campaign=Badge_Grade)

Programming challenges gathered from the Internet, and resolved by myself in different languages.