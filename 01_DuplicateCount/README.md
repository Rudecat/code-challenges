# Challenge 01 - Duplicate Count

## Description

Count the number of Duplicates
Write a function that will return the count of distinct case-insensitive alphabetic characters and numeric digits that occur more than once in the input string. The input string can be assumed to contain only alphabets (both uppercase and lowercase) and numeric digits.

## Example

1.  "abcde" -> `0 # no characters repeats more than once`
2.  "aabbcde" -> `2 # 'a' and 'b'`
3.  "aabBcde" -> `2 # 'a' occurs twice and 'b' twice ('b' and 'B')`
4.  "indivisibility" -> `1 # 'i' occurs six times`
5.  "Indivisibilities" -> `2 # 'i' occurs seven times and 's' occurs twice`
6.  "aA11" -> `2 # 'a' and '1'`
7.  "ABBA" -> `2 # 'A' and 'B' each occur twice`
