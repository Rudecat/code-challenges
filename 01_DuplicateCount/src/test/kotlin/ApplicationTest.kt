import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import kotlin.random.Random

/**
 * AutoTesting on duplicate counts challenge
 */
class CountingDuplicatesTest {

    /**
     * empty returns zero
     */
    @Test
    fun `empty returns zero`() {
        assertEquals(0, duplicateCount(""))
    }

    /**
     * no duplicate
     */
    @Test
    fun `abcde returns zero`() {
        assertEquals(0, duplicateCount("abcde"))
    }

    /**
     * one duplicate multiple times
     */
    @Test
    fun `abcdeaa returns one`() {
        assertEquals(1, duplicateCount("abcdeaa"))
    }

    /**
     * two duplicates different cases
     */
    @Test
    fun `abcdeaB returns two`() {
        assertEquals(2, duplicateCount("abcdeaB"))
    }

    /**
     * two duplicates multiple times
     */
    @Test
    fun `indivisibilities returns two`() {
        assertEquals(2, duplicateCount("Indivisibilities"))
    }

    /**
     * no duplicate with longer string
     */
    @Test
    fun `abcdefghijklmnopqrstuvwxyz returns zero`() {
        assertEquals(0, duplicateCount("abcdefghijklmnopqrstuvwxyz"))
    }

    /**
     * two duplicates with longer string
     */
    @Test
    fun `abcdefghijklmnopqrstuvwxyzaaAb returns two`() {
        assertEquals(2, duplicateCount("abcdefghijklmnopqrstuvwxyzaaAb"))
    }

    /**
     * multiple duplicates
     */
    @Test
    fun `abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz returns twenty six`() {
        assertEquals(26, duplicateCount("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"))
    }

    /**
     * multiple duplicates with different cases
     */
    @Test
    fun `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ returns twenty six`() {
        assertEquals(26, duplicateCount("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"))
    }

    /**
     * random generated tests
     */
    @Test
    fun `random tests`() {
        for (test in 1..100) {
            val dups = Random.nextInt(37)
            val used = mutableSetOf<Char>()
            fun random(): Char {
                var char: Char
                do char = Random.nextInt(36).toString(36).first() while (char in used)
                used += char
                return char
            }

            val text = ((1..dups).flatMap {
                val value = random()
                (0..Random.nextInt(1, 10)).map { if (Random.nextBoolean()) value.toUpperCase() else value }
            } + (1..Random.nextInt(37 - dups)).map { random() }).shuffled().joinToString("")
            assertEquals(dups, duplicateCount(text))
        }
    }
}