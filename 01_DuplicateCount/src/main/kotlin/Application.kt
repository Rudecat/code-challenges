/**
 * Main Function to call duplicateCount with simple sample
 */
fun main(){
    println(duplicateCount("Indivisibilities"))
}

/**
 * The function to process the input string and
 * find the count of duplicate characters in the string.
 * use groupBy to group all characters in lowercase,
 * and use filter to count the number of characters having occurrence more than one
 * @param text the input string, can only be alphabet or digit
 */
fun duplicateCount(text: String) = text.groupBy(Char::toLowerCase).count { it.value.count() > 1 }